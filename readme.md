# NP.SH browser

An alternative to NPS browser for Linux written in bash

## Description

This program is for viewing TSV file list files and downloading the entries in them in batches.

## Dependencies

* dialog
* gawk
* wget
* pkg2zip (native or via wine)

## Setup

* Edit the URLS variable to include URLs of the TSV files to view, separated by a single space
* Edit the UNPAK variable to include the command for launching pkg2zip you usually use

## Usage

* `./np.sh`
* The TSV files are being retrieved
* The search query box is presented. Enter the search query to find the necessary items in the TSV files, then select Search.
* The search results are presented. Select the ones you would like to download and mark them by pressing the space bar.
* When all the results you are interested in are selected, press "OK"
* The query box reappears so you can search for more entries in the TSV. If you are done searching, then select Finish.
* The selected entries will begin downloading and unpacking into the current directory.
* Once done, all temporary files created in the process will be cleaned up.

