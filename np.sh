#!/bin/bash

# NP.Sh Browser
# alpha version by Genjitsu Labs, 2019

# Place URLs to repositories here, separated by spaces
URLS="http://nopaystation.com/tsv/PSV_GAMES.tsv http://nopaystation.com/tsv/PSV_DLCS.tsv http://nopaystation.com/tsv/PSP_GAMES.tsv http://nopaystation.com/tsv/PSV_THEMES.tsv"
# Place your command to pkg2zip here
UNPAK="wine pkg2zip.exe -x"

##### Download repositories
tempDb=$(mktemp "/tmp/npsdatabase.XXX")
OIFS="$IFS"
IFS=" "
for url in $URLS
do
	tempRepo=$(mktemp "/tmp/npsrepo.XXX")
	wget "$url" -O "$tempRepo" 2>&1 |\
	 stdbuf -o0 gawk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3); fflush() }' |\
	  dialog --title "Fetching database..." --gauge "GET $url" 0 0
	cat "$tempRepo" >> "$tempDb"
	rm "$tempRepo"
done
IFS="$OIFS"

##### Function to parse TSV line
parse_line() {
	OIFS="$IFS"
	IFS=$'\t'
	read -a entry <<<"$1"
	TITLEID=${entry[0]}
	REGION=${entry[1]}
	TITLE=${entry[2]}
	URL=${entry[3]}
	ZRIF=${entry[4]}
	CID=${entry[5]}
	DATE=${entry[6]}
	SZ=${entry[8]}
	SHAHASH=${entry[9]}
	VER=${entry[11]}
	IFS="$OIFS"
}

##### Function to download a file from a TSV line
get_file() {
	parse_line "$1"
	tf=$(mktemp "/tmp/npsdownload$TITLEID.XXXXXX")
	wget -O "$tf" "$URL" 2>&1 |\
	 stdbuf -o0 gawk '/[.] +[0-9][0-9]?[0-9]?%/ { print substr($0,63,3); fflush() }' |\
	  dialog --title "Download in progress" --gauge "Downloading: [$TITLEID] ($REGION) $TITLE" 0 0
	eval $UNPAK "$tf" "$ZRIF" 2>&1 |\
	 stdbuf -o0 dialog --title "Download in progress" --progressbox "Unpacking: [$TITLEID] ($REGION) $TITLE" 60 60
	rm "$tf"
}

##### Variables for search interface
searchquery=" "
exitcode=0
cmd=(dialog --separate-output --checklist "Select desired downloads:" 0 0 16)

### Repeat until user pressed "Finish"
while (( exitcode == 0 ))
do
	### Prompt search query
	exec 3>&1;
	searchquery=$(dialog --title "NP.Sh Browser" --ok-label "Search" --cancel-label "Finish" --inputbox "Enter query to search or Finish to proceed with downloads" 0 0 2>&1 1>&3);
	exitcode=$?;
	exec 3>&-;
	declare -a lines
	### If there was a query
	if (( exitcode == 0 ))
	then
		### Build a list
		while read templine
		do
			parse_line "$templine"
			# TODO: Display just the file title and ID properly
			lines+=("$templine" "" off)
		done
		choices=$("${cmd[@]}" "${lines[@]}" 2>&1 >/dev/tty)
		selection+="$choices"
		selection+=$'\n'
		## Todo: Better search than just grep (i.e. auto find DLCs)
	fi <<< "$(cat "$tempDb" | grep "$searchquery")"
done

### Download all selected files
OIFS="$IFS"
IFS=$'\n'
for item in $selection
do
	if [ ! -z "$item" ]
	then
		get_file "$item"
	fi
done
IFS="$OIFS"

# Todo: put downloaded files over ftp?

### Done!
clear
echo "- Removing temporary database"
rm "$tempDb"
